package net.ch1

import java.util.UUID

object Cafe3:

  final class Coffee(val price: BigDecimal = BigDecimal.valueOf(2))

  case class Charge(val cc: CreditCard, val amount: BigDecimal):
    def combine(other: Charge): Charge =
      if (cc.id == other.cc.id) then
        Charge(cc, other.amount + amount)
      else
        throw new Exception("Can't combine charges to different cards!")

  def buyCoffee(cc: CreditCard): (Coffee, Charge) =
    val cup = new Coffee()
    (cup, Charge(cc, cup.price))

  case class CreditCard(val id: UUID)

  def buyCoffees(cc: CreditCard, n: Int): (List[Coffee], Charge) =
    val purchases: List[(Coffee, Charge)] = List.fill(n)(buyCoffee(cc))
    val unzipped: (List[Coffee], List[Charge]) = purchases.unzip
    val (coffees, charges) = unzipped
    (coffees, charges.reduce( (c1, c2) => c1.combine(c2)))