package net.ch1

import java.util.UUID

object Cafe2:

  trait Payments:
    def charge(cc: CreditCard, price: BigDecimal): Unit

  // Actually calls Credit Card Processor to make payment
  final class ProdPayments(url: String, secret: String) extends Payments:
    override def charge(cc: CreditCard, price: BigDecimal): Unit = ???

  case class Coffee(val price: BigDecimal = BigDecimal.valueOf(2))

  def buyCoffee(cc: CreditCard, p: Payments): Coffee =
    val cup = new Coffee()
    p.charge(cc, cup.price)
    cup

  val stubPayments: Payments = new Payments:
    override def charge(cc: CreditCard, price: BigDecimal): Unit = ()

  buyCoffee(CreditCard(UUID.randomUUID()), stubPayments)

  case class CreditCard(id: UUID)

// Easier to test since we can supply a stub/mock/fake Payments' instance. But the testability
//  isn't that great if we opt to check that the Payments#charge was actually called/executed. Can lead
//  to usage of heavyweight mock frameworks.
// Let's say someone wants to buy 12 cups of coffee. That'll result in 12 separate CC transaction fees!
//  This code is not modular.