package net.ch1

import java.util.UUID

object Cafe1:

  case class Coffee(val price: BigDecimal = BigDecimal.valueOf(2))

  def buyCoffee(cc: CreditCard): Coffee =
    val cup = new Coffee()
    cc.charge(cup.price)
    cup

  case class CreditCard(url: String, secret: String, id: UUID):
    // Call Credit Card processor to charge amount
    def charge(price: BigDecimal): Unit = ???

  // Hard to test since calling CreditCard#charge actually calls the credit card company to make a charge!
  // CreditCard is doing far too much:
   // (1) "CreditCard shouldn’t have any knowledge baked into it about how to contact the credit card
  // company to actually execute a charge,
  //  (2) nor should it have knowledge of how to persist a record
  // of this charge in our internal systems."