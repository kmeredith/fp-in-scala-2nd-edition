package net.ch2

object Recursion:
  def map[A, B](list: List[A], f: A => B): List[B] = list match {
    case Nil => Nil
    case hd :: tail => f(hd) :: map(tail, f)
  }

  def count[A](list: List[A]): Int = list match {
    case Nil => 0
    case _ :: tail => 1 + count(tail)
  }

