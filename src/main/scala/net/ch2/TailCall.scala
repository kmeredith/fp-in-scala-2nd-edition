package net.ch2

import scala.annotation.tailrec

object TailCall:
  def factorialTC(n: Int): BigInt =
    @tailrec
    def go(n: Int, acc: BigInt): BigInt =
      if n <= 1 then acc
      else go(n-1, n * acc)

    go(n, 1)

