package net.ch2

object NotTailCall:
  def factorial(n: Int): BigInt =
    if(n <= 1) then 1
    else n * factorial(n - 1)